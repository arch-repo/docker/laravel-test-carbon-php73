FROM registry.gitlab.com/arch-repo/docker/php73:latest

# Update
RUN pacman --noconfirm -Syu

# Install node, composer, and yarn
RUN pacman --noconfirm --needed -S nodejs-lts-carbon yarn composer base-devel python2 unzip rsync openssh

# Clean up
RUN rm -f \
      /var/cache/pacman/pkg/* \
      /var/lib/pacman/sync/* \
      /etc/pacman.d/mirrorlist.pacnew

